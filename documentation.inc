<?php

/**
 * @file
 * Create documentation links at the top of all standard content types.
 *
 * Called from within hook_form_alter(&$form, &$form_state, $form_id).
 */

// Define the machine name of every content creation page, the "friendly" name,
// and the URL to the documentation.
$documentation = [
  'uw_undergraduate_award_node_form' => [
    'name' => 'award',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/award',
  ],
  'biblio_node_form' => [
    'name' => 'bibliography',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/bibliography',
  ],
  'uw_blog_node_form' => [
    'name' => 'blog post',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/blog-post',
  ],
  'contact_node_form' => [
    'name' => 'contact',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/create-contact',
  ],
  'uwaterloo_custom_listing_node_form' => [
    'name' => 'custom listing page',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/custom-listing-page',
  ],
  'uw_embedded_call_to_action_node_form' => [
    'name' => 'embedded call to action',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/embedded-cta',
  ],
  'uw_embedded_facts_and_figures_node_form' => [
    'name' => 'embedded facts and figures',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/facts-and-figures',
  ],
  'uw_embedded_timeline_node_form' => [
    'name' => 'embedded timeline',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/embedded-timeline',
  ],
  'uw_event_node_form' => [
    'name' => 'event',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/create-event',
  ],
  'uw_home_page_banner_node_form' => [
    'name' => 'home page banner',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/home-page-banner',
  ],
  'uw_image_gallery_node_form' => [
    'name' => 'image gallery',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/create-image-gallery',
  ],
  'uw_news_item_node_form' => [
    'name' => 'news item',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/news-item',
  ],
  'uw_opportunities_node_form' => [
    'name' => 'opportunity',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/opportunity',
  ],
  'uw_ct_person_profile_node_form' => [
    'name' => 'person profile',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/person-profile',
  ],
  'uw_project_node_form' => [
    'name' => 'project',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/create-project',
  ],
  'uw_promotional_item_node_form' => [
    'name' => 'promotional item',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/promotional-items',
  ],
  'uw_service_node_form' => [
    'name' => 'service',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/service',
  ],
  'uw_ct_single_page_home_node_form' => [
    'name' => 'single page home',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/single-page/configure-your-single-page-site',
  ],
  'uw_site_footer_node_form' => [
    'name' => 'site footer',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/site-footer',
  ],
  'uw_special_alert_node_form' => [
    'name' => 'special alert',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/add-special-alert',
  ],
  'uw_web_form_node_form' => [
    'name' => 'web form',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/create-web-form',
  ],
  'uw_web_page_node_form' => [
    'name' => 'web page',
    'url' => 'https://uwaterloo.ca/web-resources/wcms-users/training-and-support/wcms-how-documents/web-page',
  ],
];

// Add documentation information if we're on a form that we have documentation
// for.
if (in_array($form_id, array_keys($documentation))) {
  // Create a prefix if one doesn't already exist. We add to it later. We want
  // to make sure we don't accidentally remove a pre-existing one.
  if (!isset($form['#prefix'])) {
    $form['#prefix'] = '';
  }
  // Generate the link.
  $link = l($documentation[$form_id]['name'] . ' documentation', $documentation[$form_id]['url'], array('attributes' => array('target' => '_blank')));
  $form['#prefix'] .= '<p class="uw-documentation">Need help? Check out our ' . $link . '. (Link will open in a new window or tab.)</p>';
}

// Add a warning if they are on "webform" instead of "web form".
if ($form_id == 'webform_node_form') {
  $form['#prefix'] = '<p class="uw-documentation">This is the wrong webform. If you are seeing this and are not an administrator, please submit an RT. <strong>Do not create webforms using this content type.</strong></p>';
}
