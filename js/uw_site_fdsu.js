/**
 * @file
 */

(function ($) {
  Drupal.behaviors.uw_site_fdsu = {
    attach: function (context, settings) {
      var mlid = Drupal.settings.uw_site_fdsu.mlid;
      $('#menu-overview input[name="mlid:' + mlid + '[mlid]"]').closest('tr').removeClass('draggable').find('.tabledrag-handle').removeClass('tabledrag-handle').find('div.handle').remove();
    }
  }
})(jQuery);
